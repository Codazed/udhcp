from __future__ import annotations

import socket
import struct

from enum import Enum
from typing import Any


def ip_from_bytes(b: bytes) -> str:
    return ".".join(map(str, struct.unpack_from("BBBB", b)))


def hardware_address_from_bytes(b: bytes, length: int) -> str:
    unpacked = struct.unpack_from(f"{length}B", b[:length])
    return ":".join(format(byte, "02x") for byte in unpacked)


def string_unpack(b: bytes) -> str:
    return struct.unpack(f"{len(b)}s", b)[0].decode()


def string_pack(string: str) -> bytes:
    return struct.pack(f"{len(string)}s", string)


def bool_unpack(b: bytes) -> bool:
    return b[0] == 1


def bool_pack(boolean: bool) -> bytes:
    return f"{int(boolean)}".encode()


def n_string_from_bytes(b: bytes) -> str:
    """Returns an expected null-terminated string from bytes"""
    null_index = b.find(b"\x00")
    max_length = null_index if null_index != -1 else len(b)
    return struct.unpack_from(f"{max_length}s", b)[0].decode("utf-8")


def unpack_ip_list(b: bytes) -> list[str]:
    chunks = [b[i : i + 4] for i in range(0, len(b), 4)]
    return [socket.inet_ntoa(struct.unpack("!BBBB", ip)) for ip in chunks]


def pack_ip_list(ip_list: list[str]) -> bytes:
    return b"".join([struct.pack("!BBBB", *[int(x) for x in ip.split(".")]) for ip in ip_list])


def unpack_client_identifier(b: bytes) -> tuple[int, str]:
    hardware_type = b[0]
    if hardware_type != 1:
        raise NotImplementedError("Unsupported hardware type", hardware_type)
    client_identifier = hardware_address_from_bytes(b[1:], len(b[1:]))
    return hardware_type, client_identifier


def pack_client_identifier(data: tuple[int, str]) -> bytes:
    bytestring = struct.pack("B", data[0])
    bytestring += string_pack(data[1])
    return bytestring


class DHCPOpCode(Enum):
    BOOT_REQUEST = 1
    BOOT_REPLY = 2


class DHCPMessageType(Enum):
    DISCOVER = 1
    OFFER = 2
    REQUEST = 3
    DECLINE = 4
    ACK = 5
    NAK = 6
    RELEASE = 7
    INFORM = 8


class DHCPOption(Enum):
    # RFC 1497 Vendor Extensions
    PAD = 0
    END = 255
    SUBNET_MASK = 1
    TIME_OFFSET = 2
    ROUTER = 3
    TIME_SERVER = 4
    NAME_SERVER = 5
    DOMAIN_NAME_SERVER = 6
    LOG_SERVER = 7
    COOKIE_SERVER = 8
    LPR_SERVER = 9
    IMPRESS_SERVER = 10
    RESOURCE_LOCATION_SERVER = 11
    HOST_NAME = 12
    BOOT_FILE_SIZE = 13
    MERIT_DUMP_FILE = 14
    DOMAIN_NAME = 15
    SWAP_SERVER = 16
    ROOT_PATH = 17
    EXTENSIONS_PATH = 18
    # IP Layer Parameters per Host
    IP_FORWARDING = 19
    NON_LOCAL_SOURCE_ROUTING = 20
    POLICY_FILTER = 21
    MAX_DATAGRAM_REASSEMBLY_SIZE = 22
    DEFAULT_IP_TTL = 23
    PATH_MTU_AGING_TIMEOUT = 24
    PATH_MTU_PLATEAU_TABLE = 25
    # IP Layer Parameters per Interface
    INTERFACE_MTU = 26
    ALL_SUBNETS_ARE_LOCAL = 27
    BROADCAST_ADDRESS = 28
    PERFORM_MASK_DISCOVERY = 29
    MASK_SUPPLIER = 30
    PERFORM_ROUTER_DISCOVERY = 31
    ROUTER_SOLICITATION_ADDRESS = 32
    STATIC_ROUTE = 33
    # Link Layer Parameters per Interface
    TRAILER_ENCAPSULATION = 34
    ARP_CACHE_TIMEOUT = 35
    ETHERNET_ENCAPSULATION = 36
    # TCP Parameters
    TCP_DEFAULT_TTL = 37
    TCP_KEEPALIVE_INTERVAL = 38
    TCP_KEEPALIVE_GARBAGE = 39
    # Application and Service Parameters
    NETWORK_INFORMATION_SERVICE_DOMAIN = 40
    NETWORK_INFORMATION_SERVERS = 41
    NETWORK_TIME_PROTOCOL_SERVERS = 42
    VENDOR_SPECIFIC_INFORMATION = 43
    NETBIOS_NAME_SERVER = 44
    NETBIOS_DATAGRAM_DISTRIBUTION_SERVER = 44
    NETBIOS_NODE_TYPE = 46
    NETBIOS_SCOPE = 47
    X_WINDOW_SYSTEM_FONT_SERVER = 48
    X_WINDOW_SYSTEM_DISPLAY_MANAGER = 49
    NETWORK_INFORMATION_SERVICE_PLUS_DOMAIN = 64
    NETWORK_INFORMATION_SERVICE_PLUS_SERVERS = 65
    MOBILE_IP_HOME_AGENT = 68
    SIMPLE_MAIL_TRANSPORT_PROTOCOL_SERVER = 69
    POST_OFFICE_PROTOCOL_SERVER = 70
    NETWORK_NEWS_TRANSPORT_PROTOCOL_SERVER = 71
    DEFAULT_WORLD_WIDE_WEB_SERVER = 72
    DEFAULT_FINGER_SERVER = 73
    DEFAULT_INTERNET_RELAY_CHAT_SERVER = 74
    STREETTALK_SERVER = 75
    STREETTALK_DIRECTORY_ASSISTANCE_SERVER = 76
    # DHCP Extensions
    REQUESTED_IP_ADDRESS = 50
    IP_ADDRESS_LEASE_TIME = 51
    OPTION_OVERLOAD = 52
    TFTP_SERVER_NAME = 66
    BOOTFILE_NAME = 67
    DHCP_MESSAGE_TYPE = 53
    SERVER_IDENTIFIER = 54
    PARAMETER_REQUEST_LIST = 55
    MESSAGE = 56
    MAX_DHCP_MESSAGE_SIZE = 57
    RENEWAL_TIME_VALUE = 58
    REBINDING_TIME_VALUE = 59
    VENDOR_CLASS_IDENTIFIER = 60
    CLIENT_IDENTIFIER = 61


# Common unpack/pack handlers
unpack_pack_ip = (lambda x: socket.inet_ntoa(x), lambda x: socket.inet_aton(x))
unpack_pack_ip_list = (lambda x: unpack_ip_list(x), lambda x: pack_ip_list(x))
unpack_pack_string = (lambda x: string_unpack(x), lambda x: string_pack(x))
unpack_pack_bool = (lambda x: bool_unpack(x), lambda x: bool_pack(x))
unpack_pack_uint8 = (lambda x: struct.unpack("B", x)[0], lambda x: struct.pack("B", x))
unpack_pack_uint16 = (lambda x: struct.unpack("H", x)[0], lambda x: struct.pack("H", x))
unpack_pack_uint32 = (lambda x: struct.unpack(">I", x)[0], lambda x: struct.pack("I", x))

options_handlers_mapping = {
    DHCPOption.SUBNET_MASK: unpack_pack_ip,
    DHCPOption.TIME_OFFSET: (lambda x: struct.unpack("i", x)[0], lambda x: struct.pack("i", x)),
    DHCPOption.ROUTER: unpack_pack_ip_list,
    DHCPOption.TIME_SERVER: unpack_pack_ip_list,
    DHCPOption.NAME_SERVER: unpack_pack_ip_list,
    DHCPOption.DOMAIN_NAME_SERVER: unpack_pack_ip_list,
    DHCPOption.LOG_SERVER: unpack_pack_ip_list,
    DHCPOption.COOKIE_SERVER: unpack_pack_ip_list,
    DHCPOption.LPR_SERVER: unpack_pack_ip_list,
    DHCPOption.IMPRESS_SERVER: unpack_pack_ip_list,
    DHCPOption.RESOURCE_LOCATION_SERVER: unpack_pack_ip_list,
    DHCPOption.HOST_NAME: unpack_pack_string,
    DHCPOption.BOOT_FILE_SIZE: unpack_pack_uint16,
    DHCPOption.MERIT_DUMP_FILE: unpack_pack_string,
    DHCPOption.DOMAIN_NAME: unpack_pack_string,
    DHCPOption.SWAP_SERVER: unpack_pack_ip,
    DHCPOption.ROOT_PATH: unpack_pack_string,
    DHCPOption.EXTENSIONS_PATH: unpack_pack_string,
    DHCPOption.IP_FORWARDING: unpack_pack_bool,
    DHCPOption.NON_LOCAL_SOURCE_ROUTING: unpack_pack_bool,
    # TODO: DHCPOption.POLICY_FILTER:
    DHCPOption.MAX_DATAGRAM_REASSEMBLY_SIZE: unpack_pack_uint16,
    DHCPOption.DEFAULT_IP_TTL: unpack_pack_uint8,
    DHCPOption.PATH_MTU_AGING_TIMEOUT: unpack_pack_uint32,
    # TODO: DHCPOption.PATH_MTU_PLATEAU_TABLE:
    DHCPOption.INTERFACE_MTU: unpack_pack_uint16,
    DHCPOption.ALL_SUBNETS_ARE_LOCAL: unpack_pack_bool,
    DHCPOption.BROADCAST_ADDRESS: unpack_pack_ip,
    DHCPOption.MASK_SUPPLIER: unpack_pack_bool,
    DHCPOption.PERFORM_ROUTER_DISCOVERY: unpack_pack_bool,
    DHCPOption.ROUTER_SOLICITATION_ADDRESS: unpack_pack_ip,
    # TODO: DHCPOption.STATIC_ROUTE:
    DHCPOption.TRAILER_ENCAPSULATION: unpack_pack_bool,
    DHCPOption.ARP_CACHE_TIMEOUT: unpack_pack_uint32,
    DHCPOption.ETHERNET_ENCAPSULATION: unpack_pack_bool,
    DHCPOption.TCP_DEFAULT_TTL: unpack_pack_uint8,
    DHCPOption.TCP_KEEPALIVE_INTERVAL: unpack_pack_uint32,
    DHCPOption.TCP_KEEPALIVE_GARBAGE: unpack_pack_bool,
    DHCPOption.NETWORK_INFORMATION_SERVICE_DOMAIN: unpack_pack_string,
    DHCPOption.NETWORK_INFORMATION_SERVERS: unpack_pack_ip_list,
    DHCPOption.NETWORK_TIME_PROTOCOL_SERVERS: unpack_pack_ip_list,
    # TODO: DHCPOption.VENDOR_SPECIFIC_INFORMATION:
    DHCPOption.NETBIOS_NAME_SERVER: unpack_pack_ip_list,
    DHCPOption.NETBIOS_DATAGRAM_DISTRIBUTION_SERVER: unpack_pack_ip_list,
    DHCPOption.NETBIOS_NODE_TYPE: unpack_pack_uint8,
    DHCPOption.NETBIOS_SCOPE: unpack_pack_string,
    DHCPOption.X_WINDOW_SYSTEM_FONT_SERVER: unpack_pack_ip_list,
    DHCPOption.X_WINDOW_SYSTEM_DISPLAY_MANAGER: unpack_pack_ip_list,
    DHCPOption.NETWORK_INFORMATION_SERVICE_PLUS_DOMAIN: unpack_pack_string,
    DHCPOption.NETWORK_INFORMATION_SERVICE_PLUS_SERVERS: unpack_pack_ip_list,
    DHCPOption.MOBILE_IP_HOME_AGENT: unpack_pack_ip_list,
    DHCPOption.SIMPLE_MAIL_TRANSPORT_PROTOCOL_SERVER: unpack_pack_ip_list,
    DHCPOption.POST_OFFICE_PROTOCOL_SERVER: unpack_pack_ip_list,
    DHCPOption.NETWORK_NEWS_TRANSPORT_PROTOCOL_SERVER: unpack_pack_ip_list,
    DHCPOption.DEFAULT_WORLD_WIDE_WEB_SERVER: unpack_pack_ip_list,
    DHCPOption.DEFAULT_FINGER_SERVER: unpack_pack_ip_list,
    DHCPOption.STREETTALK_SERVER: unpack_pack_ip_list,
    DHCPOption.STREETTALK_DIRECTORY_ASSISTANCE_SERVER: unpack_pack_ip_list,
    DHCPOption.REQUESTED_IP_ADDRESS: unpack_pack_ip,
    DHCPOption.IP_ADDRESS_LEASE_TIME: unpack_pack_uint32,
    DHCPOption.OPTION_OVERLOAD: unpack_pack_uint8,
    DHCPOption.TFTP_SERVER_NAME: unpack_pack_string,
    DHCPOption.BOOTFILE_NAME: unpack_pack_string,
    DHCPOption.DHCP_MESSAGE_TYPE: (
        lambda x: DHCPMessageType(struct.unpack("B", x)[0]),
        lambda x: struct.pack("B", x.value),
    ),
    DHCPOption.SERVER_IDENTIFIER: unpack_pack_ip,
    DHCPOption.PARAMETER_REQUEST_LIST: (
        lambda x: list(map(DHCPOption, [*struct.unpack(f"{len(x)}B", x)])),
        lambda x: struct.pack("B", *[y.value for y in x]),
    ),
    DHCPOption.MESSAGE: unpack_pack_string,
    DHCPOption.MAX_DHCP_MESSAGE_SIZE: unpack_pack_uint16,
    DHCPOption.RENEWAL_TIME_VALUE: unpack_pack_uint32,
    DHCPOption.REBINDING_TIME_VALUE: unpack_pack_uint32,
    DHCPOption.VENDOR_CLASS_IDENTIFIER: unpack_pack_string,
    DHCPOption.CLIENT_IDENTIFIER: (unpack_client_identifier, pack_client_identifier),
}


def parse_options(options_data: bytes) -> dict[DHCPOption, Any]:
    options_dict: dict[DHCPOption, Any] = {}
    current_index = 0
    while True:
        code = DHCPOption(options_data[current_index])
        if code == DHCPOption.END:
            break
        current_index += 1
        length = options_data[current_index]
        current_index += 1
        options_dict[code] = options_handlers_mapping[code][0](options_data[current_index : current_index + length])
        current_index += length
    return options_dict


class DHCPPacket:
    op: DHCPOpCode
    hardware_address_type: int
    hardware_address_len: int
    hops: int
    transaction_id: int
    seconds_elapsed: int
    flags: int
    client_ip: str
    your_ip: str
    next_server_ip: str
    relay_agent_ip: str
    client_hardware_address: str
    server_host_name: str
    boot_file: str
    options: dict[DHCPOption, Any]

    @staticmethod
    def from_bytes(raw: bytes) -> DHCPPacket:
        packet = DHCPPacket()

        packet.op = DHCPOpCode(struct.unpack_from("B", raw[:1])[0])
        packet.hardware_address_type = struct.unpack_from("B", raw[1:2])[0]
        packet.hardware_address_len = struct.unpack_from("B", raw[2:3])[0]
        packet.hops = struct.unpack_from("B", raw[3:4])[0]
        packet.transaction_id = struct.unpack_from(">I", raw[4:8])[0]
        packet.seconds_elapsed = struct.unpack_from(">H", raw[8:10])[0]
        packet.flags = struct.unpack_from(">H", raw[10:12])[0]
        packet.client_ip = ip_from_bytes(raw[12:16])
        packet.your_ip = ip_from_bytes(raw[16:20])
        packet.next_server_ip = ip_from_bytes(raw[20:24])
        packet.relay_agent_ip = ip_from_bytes(raw[24:28])
        packet.client_hardware_address = hardware_address_from_bytes(raw[28:44], packet.hardware_address_len)
        packet.server_host_name = n_string_from_bytes(raw[44:108])
        packet.boot_file = n_string_from_bytes(raw[108:236])
        # DHCP magic cookie should be in raw[236:240]
        packet.options = parse_options(raw[240:])

        return packet
