from udhcp import udhcp

with open("samples/dhcp_ack_packet", "rb") as f:
    raw_packet = f.read()

packet = udhcp.DHCPPacket.from_bytes(raw_packet)


def test_op_code():
    assert packet.op == udhcp.DHCPOpCode.BOOT_REPLY


def test_hardware_address_type():
    assert packet.hardware_address_type == 0x01


def test_hardware_address_length():
    assert packet.hardware_address_len == 6


def test_hops():
    assert packet.hops == 0


def test_transaction_id():
    assert packet.transaction_id == 0x00003D1E


def test_seconds_elapsed():
    assert packet.seconds_elapsed == 0


def test_flags():
    assert packet.flags == 0x0000


def test_client_ip():
    assert packet.client_ip == "0.0.0.0"


def test_your_ip():
    assert packet.your_ip == "192.168.0.10"


def test_next_server_ip():
    assert packet.next_server_ip == "0.0.0.0"


def test_relay_agent_ip():
    assert packet.relay_agent_ip == "0.0.0.0"


def test_client_hardware_addr():
    assert packet.client_hardware_address == "00:0b:82:01:fc:42"


def test_server_host_name():
    assert packet.server_host_name == ""


def test_boot_file_name():
    assert packet.boot_file == ""


def test_options():
    assert packet.options[udhcp.DHCPOption.DHCP_MESSAGE_TYPE] == udhcp.DHCPMessageType.ACK
    assert packet.options[udhcp.DHCPOption.RENEWAL_TIME_VALUE] == 1800
    assert packet.options[udhcp.DHCPOption.REBINDING_TIME_VALUE] == 3150
    assert packet.options[udhcp.DHCPOption.IP_ADDRESS_LEASE_TIME] == 3600
    assert packet.options[udhcp.DHCPOption.SERVER_IDENTIFIER] == "192.168.0.1"
    assert packet.options[udhcp.DHCPOption.SUBNET_MASK] == "255.255.255.0"
